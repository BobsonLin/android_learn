package com.example.bobson.miwok;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
//import android.net.Uri;
import android.net.Uri;
import android.util.Log;

/**
 * Created by Bobson on 11/20/2017.
 */

public class BootBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
            //Intent.ACTION_BOOT_COMPLETED == android.intent.action.BOOT_COMPLETED

            Log.w("BootBroadcastReceiver", "Receive ACTION_BOOT_COMPLETED!!!");
            Uri uri = Uri.parse("https://demo.aiwill.tech");
            Intent intent1 = new Intent(Intent.ACTION_VIEW, uri);
//            Intent intent1 = new Intent(context , MainActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent1);
            //執行一個Activity

//            Intent intent2 = new Intent(context , MyService.class);
//            context.startService(intent2);
//            //執行一個Service
        }
    }
}
